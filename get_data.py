from zeep import Client
from datetime import datetime


def get_rates(client: Client, dt_start: datetime, dt_end: datetime, currency: str):
    result_xml = client.service.GetCursDynamicXML(FromDate=dt_start, ToDate=dt_end, ValutaCode=currency)
    for child in result_xml:
        date = child.find('CursDate').text
        value = child.find('Vcurs').text
        yield date.strip(), float(value)


def get_currency_codes(client: Client):
    result_xml = client.service.EnumValutesXML(Seld=False)
    response = {}
    for child in result_xml:
        char_code = child.find('VcharCode')
        code = child.find('Vcode')
        if char_code is not None and code is not None:
            response[char_code.text.strip()] = code.text.strip()
    return response


cl = Client('https://www.cbr.ru/DailyInfoWebServ/DailyInfo.asmx?WSDL')
start_time = datetime(2018, 10, 5)
end_time = datetime(2018, 10, 10)
codes = get_currency_codes(cl)
data = {}
for dt, v in get_rates(cl, start_time, end_time, codes['USD']):
    data[dt] = {'USD': v}
for dt, v in get_rates(cl, start_time, end_time, codes['EUR']):
    data[dt]['EUR'] = v
for rec in data:
    dt = datetime.fromisoformat(rec).strftime("%d.%m.%Y")
    usd = data[rec]['USD']
    eur = data[rec]['EUR']
    print(f'На дату {dt} 1 доллар США = {usd:.2f} руб., 1 евро = {eur:.2f} руб.')
